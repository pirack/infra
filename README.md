# infra

## Getting Started

* [Set up your Pis](pisetup.md)
* Install [ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
* Run the playbook

`ansible-playbook -i hosts playbook.yml --ask-pass -c paramiko`

* Install weave
* Dashboard install

`kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard-arm-head.yaml`