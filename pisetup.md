# Setting up your Pis

* Etcher to burn image
* Set ips/hostnames/locale (raspi-config)
* Edit `/boot/cmdline.txt`
* Add `cgroup_memory=1` and save
* Reboot
